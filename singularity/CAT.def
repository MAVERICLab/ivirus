# Copyright (c) 2015-2016, Gregory M. Kurtzer. All rights reserved.
#
# "Singularity" Copyright (c) 2016, The Regents of the University of California,
# through Lawrence Berkeley National Laboratory (subject to receipt of any
# required approvals from the U.S. Dept. of Energy).  All rights reserved.

BootStrap: debootstrap
OSVersion: stable
MirrorURL: http://ftp.us.debian.org/debian/

%runscript
    exec CAT "$@"

%environment
    export PATH=/miniconda3/bin:$PATH

%post
    apt-get update
    apt-get install -y automake build-essential wget git

    # Clean up
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

    # We need python, and not that silly python that comes with apt
    wget --no-verbose https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
    bash Miniconda3-latest-Linux-x86_64.sh -b -f -p /miniconda3/
    rm Miniconda3-latest-Linux-x86_64.sh

    conda install -c bioconda cat

    # Databases can now be pointed to on the command line. No longer need to download
    conda clean --yes --tarballs --packages --source-cache

    # TACC's Stampede compliant
    mkdir /home1 && mkdir /scratch && mkdir /work
