# Copyright (c) 2015-2016, Gregory M. Kurtzer. All rights reserved.
# 
# "Singularity" Copyright (c) 2016, The Regents of the University of California,
# through Lawrence Berkeley National Laboratory (subject to receipt of any
# required approvals from the U.S. Dept. of Energy).  All rights reserved.

BootStrap: debootstrap
OSVersion: stable
MirrorURL: http://ftp.us.debian.org/debian/


%runscript
    exec bowtie_batch.py "$@"


%post
    echo "Hello from inside the container"
    apt-get update
    apt-get install -y automake build-essential zlib1g-dev libncurses5-dev wget unzip
    
    # Clean up
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
    
    wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
    bash Miniconda2-latest-Linux-x86_64.sh -b -f -p /usr/local/
    rm Miniconda2-latest-Linux-x86_64.sh
    
    conda install -y biopython
    
    # Get natsort, biopython, and Levenshtein
    pip install natsort
    pip install python-Levenshtein
    
    export BINPATH=/usr/local/bin
    
    # Install Bowtie2
    wget --no-verbose http://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.2.6/bowtie2-2.2.6-linux-x86_64.zip
    unzip bowtie2-2.2.6-linux-x86_64.zip && cp bowtie2-2.2.6/bowtie2* $BINPATH && cd /

    # Install Samtools
    wget --no-verbose https://github.com/samtools/samtools/releases/download/1.3/samtools-1.3.tar.bz2
    tar xf samtools-1.3.tar.bz2 && cd samtools-1.3 && make && make install && cd /
    
    export BITBUCKET=https://bitbucket.org/MAVERICLab/docker-bowtiebatch/raw/beb39333f41289e2be429add4badfc2b82aa427d
    
    wget --no-verbose -P $BINPATH $BITBUCKET/scripts/bowtie_batch.py
    chmod +x $BINPATH/bowtie_batch.py
    
    # Set bind points
    mkdir /work
    mkdir /scratch
    mkdir /home1