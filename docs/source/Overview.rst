Introduction and Overview
===========================

Based on the existing CyVerse cyberinfrastructure we are developing tools, data and metadata resources specific to
viral ecology through the iVirus project. This project focuses on challenges unique to viral biology, by developing
tools specific to viruses and metagenomics to enhance data reuse and collaboration among viral researchers. The CyVerse
cyberinfrastructure promotes efficient data sharing, use of common compute resources, and a platform for developers to
securely build new Apps and data processing pipelines for viral ecology. Specifically, we are:

1. Developing Apps and data pipelines to analyzing large-scale viral and metagenomic datasets
2. Integrating disparate viral datasets in the iVirus Data Commons
3. Using metadata capabilities and standard ontologies in the CyVerse cyberinfrastructure to enhance data and software
discovery and reuse


This work is a product of the `Hurwitz Lab <http://hurwitzlab.org/>`_ at the The University of Arizona and the `Sullivan Lab <http://u.osu.edu/viruslab/>`_
at The Ohio State University.


Full Disclosure on App Selection
--------------------------------

Apps selected for inclusion in iVirus (and therefore CyVerse, and maybe KBase) is somewhat biased by the developer's
background and experience using these tools, not to mention licensing agreements. All tools have been used by the
developers, on both local and HPC environments. *Most* tools have been benchmarked, or at the very least used under a
number of different parameters for a wide variety of data types. In many cases, recommended parameters are taken from
the literature (cited where appropriate), though not every app is as highly "favored" as others. The apps selected are
commonly those that have worked well for the Sullivan lab under a variety of conditions for their data. Each iVirus
user *may not have the same experience*, so if one of the apps (ex: an assembler) doesn't work for a user's read data,
then trying another is totally worth doing. iVirus seeks to bring *a lot* of useful viral ecology apps to the
community - it won't get them all and might not be able to keep up with every tool published - but it'll do what it
can with its available resources.

**If a user has a recommendation for a tool, or is an author themselves and would like to see their app included,
please do not hesitate to contact us!**

Authorship and Citations
------------------------

iVirus is built on tools developed by the Sullivan and Hurwitz labs, and expanded through inclusion of other, 3rd party
tools. Anyone using iVirus tools (and the underlying programs) are asked to cite:

iVirus: Facilitating new insights in viral ecology with software and community data sets imbedded in a
cyberinfrastructure. (2017) Bolduc, B., Youens-Clark, K., Roux, S., Hurwitz, B.L., and Sullivan, M.B. ISME Journal

Additionally, nearly all tools have some sort of reference that can be cited. These should be cited where appropriate.
Citation information is included with every app, and here as well. If an author wishes to update their tool's citation
or adjust how it's used, please let us know.
