.. iVirus documentation master file, created by
    sphinx-quickstart on Thu Nov  1 14:39:21 2018.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

Welcome to iVirus's documentation!
==================================

iVirus is “a community resource that leverages the CyVerse cyberinfrastructure to provide access to viromic tools and data sets.”
We're focused on 3 major areas:

1. Provide *easy*-to-use tools/apps through `CyVerse <https://www.cyverse.org/>`_ and *now* KBase! (wahoo!)
2. Detailed protocols through `Protocols.io <https://protocols.io/>`_.
3. Databases and collections of “useful” viral datasets to the broader research community on CyVerse and KBase.

We won't go into much detail on the "science" side of iVirus - for that - check the references below and the those associated with specific tools.

This site is mainly to provide a singular resource for the code and links to use and/or understand the tools. If you're
here and don't want coding stuff but want to be able to run the tools you find on CyVerse, then...

`Go here <https://www.protocols.io/groups/ivirus/protocols>`_ for a collection of iVirus related protocols.

`Go here <http://ivirus.us/>`_ was the original location for iVirus. It's not code-heavy, and was designed while iVirus
(and its sister site, iMicrobe) was in its infancy. Over the years iVirus has grown onto multiple platforms, each with its
own documentation requirements, and users - from non-informatician scientists to full-blown CS programmers - want info
at the level of their appropriate backgrounds. Unfortunately, there's no single-page solution (or maybe there is, contact
me if you are aware of one) to integrating all iVirus tools, documentation, historical background, among others. Sadly,
there's only so many websites (nearly a dozen DIFFERENT PLACES) one can manage simultaneously and keep up-to-date, and
iVirus.us has fallen a bit out of date. While we try to keep it updated, it's usually the last place. So look to this
site for the most recent information, alongside protocols.io.


.. toctree::
    :maxdepth: 2
    :caption: Contents:

    Overview
    Apps-and-Tools
    Protocols
    Singularity-101
    Building-A-Pipeline

References:
------------
- **﻿iVirus: Facilitating new insights in viral ecology with software and community data sets imbedded in a cyberinfrastructure**. (2017) Bolduc, B., Youens-Clark, K., Roux, S., Hurwitz, B.L., and Sullivan, M.B. ISME Journal

Funding:
--------
We'd be remiss if we didn't mention the support of awards that make iVirus possible. iVirus wouldn't be possible
without the following grants: Gordon and Betty Moore Foundation Investigator Award (#3790), and since Sept 2018 an NSF
Advances in Biological Infrastructure Award (#1759874).

The NSF award page is `here <https://nsf.gov/awardsearch/showAward?AWD_ID=1759874&HistoricalAwards=false>`_

Thanks so much for making this work possible!

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
